
/*jslint node: true */

'use strict';

var express = require('express');
var http = require('http');
var ws = require('websocket');

var app = express();
var server = http.createServer( app );
var log = function( msg, level ){
	console.log((new Date()) + '\t'+msg );
};

// Get port number from eviroment
app.set('port', (process.env.PORT || 5000));
// As http is server which serve only static files (nginx do this better)
app.use("/", express.static(__dirname + '/static'));

server.listen( app.get('port') );
// Create instance of websocket server
var wsServer = new ws.server({ httpServer: server, autoAcceptConnections:true});

log('start');

// define function for send data to all connections.
// TODO: return promise or get callback whick call after send all and arguments are how many successed sending was. 
var broadcast = function( data ){
	var c = wsServer.connections,l=c.length;
	for(var i=0;i<l;i++){
		// TODO: add calback which count successed sending and call "next" after all
		c[i].send( data );
	}
	console.log( 'retranslated to ',l);
};

// object - is mapping of nickName to connection with him.
var userConnectionMap = {};
// check valid username
var reUsername = /^[a-zA-ZА-Яа-я0-9_-]+$/;
wsServer.on('connect', function( request ){
	//console.log( request );
//	log('Connection from origin ' + request );
//	var connection = request.accept('json-chat', request.origin);
	request.on('message', function(ev){
		if( ev.type != 'utf8' ){
			console.log( 'Unexpected message type:', ev );
			return;
		}
		var chatMsg = JSON.parse( ev.utf8Data );
		if( chatMsg.action == 'login' ){
			request.username = chatMsg.user;
			// check is username already exists or has invalid char 
			if( userConnectionMap[ request.username ] || !reUsername.test( request.username ) ){
				// send responce with error
				request.send( JSON.stringify({action:'error', user:request.username,ts:(new Date()).getTime(), data: 'Username already used or invalid' }), function(err){
					// after send set dummy username for this connection and close it
					request.username = '***';
					request.selfclosed = true;
					request.close();
				} );
				return;
			}
			// if all right - save conenction to mapping to nickname
			userConnectionMap[ chatMsg.user ] = request;
			request.loginTime = new Date();
			// send all user names in system in responce:
			// TODO: send full information about user - IP adress, login time, etc.
			var users = wsServer.connections.map(function(a){ return a.username; });
			request.send( JSON.stringify({action:'userlist', user:request.username,ts:(new Date()).getTime(), data: users }), function(){
				// and after that send all about new user (original "login" message)
				broadcast( ev.utf8Data );
			} );
		}else{
			// if it isn't "login" messgae - just retraslate it to all (include sender)
			broadcast( ev.utf8Data );
		}
	});
	// When connection closed - it means user leave chat. (or we are reject it by invalid login)
	request.on('close', function(ev){
		console.log('close', ev);
		if( request.selfclosed ) return; // Closing was by rejecting
		// clear nickname to next use.
		userConnectionMap[ request.username ] = null;
		// message about user leaviing to all other users:
		broadcast( JSON.stringify({ action:'logout', user:request.username,ts:(new Date()).getTime(), data:ev.toString() }));
	});
	request.on('error', function(ev){
		// TODO: do something.
		console.log('error', ev);
	});
});
