
var cMainApp = function(){
    var self = this;
    // Prepare jQuery variables:

    var $dialog = $('.dialog');
    var $users = $('.users');
    var $msg = $('#msg');

    // internal functions for use:
    var html_clear = function( raw ){
        return raw.replace(/</g,'&lt;').replace(/>/g,'&gt;');
    };

    // function start chat and contain all chatting logic.
    var startChat = function(){
        // create connection to server
        // TODO: handle error
        var socket = new WebSocket( (window.location.origin +'/').replace('http://','ws://')+'chat' );
        // there redefine sendMsg method with used this new connection to server.  
        self.sendMsg = function( action, data, extra ){
            // todo: check state, queue messages, analise sending result, reflect result in interface 
            socket.send( JSON.stringify({ action:action, user:self.my_name, data:data, ts:(new Date()).getTime() }));
        };
        console.log( socket );
        // handler of incoming messages:
        socket.onmessage = function( event ){
            // TODO: hadle JSON parse errors 
            var json = JSON.parse( event.data );

            json.isMine = json.user == self.my_name ;
            if( 'login' == json.action ){
                // someone login - append him to userlist and show message about it in main chat
                if( !json.isMine ) $users.append('<a href="#" u="'+json.user+'">'+json.user+'</a>');
                $dialog.append('<div class="login">&gt;&gt;&gt; '+json.user+'</div>');
            }else if( 'logout' == json.action ){
                // someone leave - remove him from userlist and show message about it in main chat
                $users.find('[u='+json.user+']').remove();
                $dialog.append('<div class="logout">&lt;&lt;&lt; '+json.user+'</div>');
            }else if( 'error' == json.action ){
                // just show error
                $dialog.append('<div class="error">'+json.data+'</div>');
            }else if( 'type' == json.action ){
                // someone typed message
                // TODO: message may be privated (only one to one) - highlight it
                var htmlClass = json.isMine ? 'mine' :'';
                if( json.recepient ){
                    htmlClass += ' private';
                }
                $dialog.append('<div '+(htmlClass?' class="'+htmlClass+'"':'')+'><span class="user" u="'+json.user+'">'+json.user+':</span><span class="msg">'+html_clear(json.data)+'</span></div>');
            }else if( 'userlist' == json.action ){
                // after login server send me a userlist: fill userlist in window by incoming data
                $users.append( json.data.map( function(a){ return '<a href="#" u="'+a+'">'+a+'</a>'; }).join('') );
            }else{
                // TODO: other message types: "user are typing", "user stop typing", "user details"
                console.log( json );
            }
            $dialog.scrollTop(100000000);
        };
        socket.onerror = function( event ){
            // TODO: do something
            console.log('error', event );
        };
        // after connetction created - send login
        socket.onopen = function( event ){
            console.log('connect', event );
            self.sendMsg('login',null);
        };
        // if connection closed - hide interface of sending message and show starting (login enterance)
        socket.onclose = function( event ){
            $('.type').hide();
            $('.start').show();
            console.log( 'Close:', event );
        };
    };
    // //////// Init part:
    // define event handlers of html elements

    // After user login - must some elements show, some elements hide, and start chat
    $('.start form').on('submit', function(ev){
        self.my_name = $('#nik').val();
        $('.room').show();
        $('.type').show();
        $('.start').hide();
        $users.html('');
        $msg.focus();
        startChat();
        ev.preventDefault();
        return false;
    });
    // when user typed some message - send it to all (to server)
    $('.type form').on('submit', function(ev){

        ev.preventDefault();
        var msg = $msg.val();
        if(!msg) return false;
        self.sendMsg('type', msg );
        $msg.val('');
        $msg.focus();
        return false;

    });

};
cMainApp.prototype.sendMsg = function( type, msg ){ // dummy function for case when something go wrong way
    console.log( 'no connection to send message');
};
// Create application instance:
var app = new cMainApp();
        